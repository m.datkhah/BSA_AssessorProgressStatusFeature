In order to develop the system Blockchain-Security-Assessment-Project(BSA) Feature entitled BSA_Assessor Progress Status
Added to the system using Laravel framework. With this feature, the user is informed about the status of his evaluation.
The whole point is that the evaluator or user answers a series of questions.These questions are grouped into three general topics, and each topic has its own sub-categories. For each sub-category, questions are asked that are different from the other sub-categories and there is a possibility of overlap between the questions.To separate the questions of each topic, we created a field in the question table called type of decimal type and categorized the three topics in this way with decimal numbers.
- Topic 1=>11
- Topic 2=>1111
- Topic 3=>111111

For example, topic 1 has 5 sub-categories, naming will be like this

- 11
- 12
- 13
- 14
- 15

Subcategory 12 can have any number of questions
So this way you can find out how many questions the subdirectory has in the database.
Now we want to let the evaluator know that you have answered the questions in topic one and let him / her know about our evaluation process.
To do this, the following  features are used
- observer
- chain of responsibility
- scope
- relations
- object oriented

Three fields are created in the userState table that show the number of questions answered in these three fields.
To do this, respondObserver
We created and used the create method of this class
By creating each answer, an example of that answer is provided by the create method. Well, using the relation between the question table and the answers, we have access to the id, type field of the answered question.
Using the strlen method, which is one of the predefined php methods, the number of characters in the type field is determined, and based on the number of characters, we can find out which topic the question is related to, and thus the desired field from the userState table is a unit. Is added to its value.
This is the next topic
With each eternity of the userState table, we want to check whether the value of those three fields has reached the quorum so that we can assign a badge to the evaluator or not?
To do this, we create a userStateObserver and use the update method of this class to check the first field once with each change, then the second field and then the third field, so we need to create a chain that executes with each change of these three chains. If the quorum is reached, a badge will be assigned to the evaluator. On the other hand, we have prevented duplicate badge assignment by using the diff method.
