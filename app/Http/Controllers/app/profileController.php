<?php

namespace App\Http\Controllers\app;

use App\Badge;
use App\Http\Controllers\Controller;
use App\Question;
use App\Response;
use Illuminate\Http\Request;

class profileController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function showProfile()
    {

        return view('layouts.profile');
    }

    public function showChecklists()
    {
       return view('profiles.checklists.checklistsMy');
    }

    public function showChecklistsNetwork()
    {
        return view('profiles.checklists.checklist-network');
    }

    public function ShowFormAddChecklistsNetwork()
    {
        return view('profiles.checklists.AddChecklistNetwork');
    }
    public function ShowFormAddChecklistsSoftware()
    {
        return view('profiles.checklists.AddChecklistSoftware');
    }
    public function ShowFormAddChecklistsHardware()
    {
        return view('profiles.checklists.AddChecklistHardware');
    }

    public function showChecklistsSoftware()
    {
        return view('profiles.checklists.checklist-software');
    }
    public function showChecklistsHardware()
    {
        return view('profiles.checklists.checklist-hardware');
    }

    public function storeChecklist(Request $request)
    {

        $this->validateStore($request);
        auth()->user()->question()->create([
            'user_id'=>auth()->user()->id,
            'text'=>$request->text,
            'type'=>$request->network
        ]);
        return back()->with('successSubmit',true);
    }

    public function validateStore($request)
    {
     return   $request->validate([
            'network'=>['required'],
            'text'=>['required']
        ]);

    }

    public function storeResponse(Request $request, Response $response)
    {
        $response->create([
            'user_id' => auth()->user()->id,
            'question_id' => $request->qid,
            'Short answer' => $request->shortAnswer,
            'text' => $request->text,

        ]);
         return back()->with('createSuccess',true);
    }

    public function newBadge()
    {
        return view('profiles.checklists.AddBadge');
    }

    public function storeBadge(Request $request)
    {
     // dd($request);
        $this->validateBadge($request);
         Badge::create([
                 'title'=>$request->title,
                 'description'=>$request->description
         ]);
        return back()->with('success',true);
    }

    public function validateBadge($request)
    {
      return  $request->validate([
            'title'=>['required'],
            'description'=>['required']
        ]);
}


    public function showChecklistNetwork(Request $request)
    {

        $questions=Question::where('type',$request->type)->get();
        return view('profiles.checklists.showchecklist-network',compact('questions'));
    }


    public function showChecklistHardware(Request $request)
    {

        $questions=Question::where('type',$request->type)->get();

        return view('profiles.checklists.showchecklist-hardware',compact('questions'));
    }
    public function showChecklistSoftware(Request $request)
    {

        $questions=Question::where('type',$request->type)->get();
        return view('profiles.checklists.showchecklist-software',compact('questions'));
    }

    public function removeResponse($id)
    {

        Response::findorfail($id)->delete();
        return back();
    }
}
