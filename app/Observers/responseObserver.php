<?php

namespace App\Observers;

use App\Question;
use App\Response;

class responseObserver
{
    /**
     * Handle the response "created" event.
     *
     * @param  \App\Response  $response
     * @return void
     */
    public function created(Response $response)
    {

  $question=Question::find($response->question_id);

        if (strlen($question->type)<=2){

            $response->user->incrementNetwork();
        }elseif (strlen($question->type)<=4){

            $response->user->incrementSoftware();
        }elseif(strlen($question->type)<=6){

            $response->user->incrementHardware();
        }

    }

    /**
     * Handle the response "updated" event.
     *
     * @param  \App\Response  $response
     * @return void
     */
    public function updated(Response $response)
    {
        //
    }

    /**
     * Handle the response "deleted" event.
     *
     * @param  \App\Response  $response
     * @return void
     */
    public function deleted(Response $response)
    {
        //
    }

    /**
     * Handle the response "restored" event.
     *
     * @param  \App\Response  $response
     * @return void
     */
    public function restored(Response $response)
    {
        //
    }

    /**
     * Handle the response "force deleted" event.
     *
     * @param  \App\Response  $response
     * @return void
     */
    public function forceDeleted(Response $response)
    {
        //
    }
}
