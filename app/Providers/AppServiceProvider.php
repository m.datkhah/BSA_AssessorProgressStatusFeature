<?php

namespace App\Providers;

use App\Observers\responseObserver;
use App\Observers\UserObserver;
use App\Observers\userStateObserver;
use App\Response;
use App\User;
use App\Userstate;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        User::observe(UserObserver::class);
        Response::observe(responseObserver::class);
        Userstate::observe(userStateObserver::class);
    }
}
