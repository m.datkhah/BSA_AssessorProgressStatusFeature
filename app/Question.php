<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
   protected $fillable=['user_id','text','type'];

    public function user()
    {
       return $this->belongsTo(User::class);
   }

    public function responses()
    {
     return  $this->hasMany(Response::class);
   }

    public function checkType($response)
    {
      if (strlen($question->type)==3){
         $this->user->incrementNetwork();
      }
       if (strlen($question->type)==4){
         $this->user->incrementSoftware();
      }
       if (strlen($question->type)==5){
         $this->user->incrementHardware();
      }

   }

}
