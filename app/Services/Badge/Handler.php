<?php
namespace App\Services\Badge;
use App\Userstate;

interface Handler {
    public function setNext(Handler  $handler);
    public function handle(Userstate $userstate);
}
