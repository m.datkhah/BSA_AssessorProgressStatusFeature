<?php
namespace App\Services\Badge;
use App\Badge;
use App\Services\Badge\AbstractHandler;
use App\Userstate;

class HardwareHandler extends AbstractHandler{
    public function handle(Userstate $userState)
    {
        if ($userState->isDirty('questionHardware_count')){
            $this->applayBadge($userState);
        }
        return parent::handle($userState);
    }
    public function applayBadge($userState)
    {
        $availabelBadge=Badge::hardware()->where('required_number','<=',$userState->questionHardware_count)->get();
        $userBadge=$userState->user->badges;
        $notRecievedBadge=$availabelBadge->diff($userBadge);

        if ( $notRecievedBadge->isEmpty()){return ;}
        $userState->user->badges()->attach($notRecievedBadge);
    }

}
