<?php
namespace App\Services\Badge;
use App\Badge;
use \App\Services\Badge\AbstractHandler;
use App\Userstate;

class SoftwareHandler extends AbstractHandler {

    public function handle(Userstate $userState)
    {
        if ($userState->isDirty('questionSoftware_count')){
            $this->applayBadge($userState);
        }
        return parent::handle($userState);
    }
    public function applayBadge($userState)
    {
        $availabelBadge=Badge::software()->where('required_number','<=',$userState->questionSoftware_count)->get();
        $userBadge=$userState->user->badges;
        $notRecievedBadge=$availabelBadge->diff($userBadge);
        if ( $notRecievedBadge->isEmpty()){return ;}
        $userState->user->badges()->attach($notRecievedBadge);

    }

}
