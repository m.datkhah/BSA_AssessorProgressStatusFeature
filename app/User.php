<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','phone_number','remember_token'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function question()
    {
       return $this->hasMany(Question::class);
    }

    public function userstate()
    {
     return   $this->hasOne(Userstate::class);
    }

    public function incrementNetwork()
    {
        $this->userstate->questionNetwork_count++;
        $this->userstate->save();
    }
    public function incrementSoftware()
    {
        $this->userstate->questionSoftware_count++;
        $this->userstate->save();
    }
    public function incrementHardware()
    {
        $this->userstate->questionHardware_count++;
        $this->userstate->save();
    }

    public function Badges()
    {
      return $this->belongsToMany(Badge::class)->withTimestamps();
    }

}
