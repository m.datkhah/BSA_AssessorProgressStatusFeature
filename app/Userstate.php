<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Userstate extends Model
{
    public function user()
    {
        return $this->belongsTo(User::class);
   }
}
