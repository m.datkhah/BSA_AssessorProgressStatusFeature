<?php
return [
    'home'=>'خانه',
    'aboutChecklist'=>'درباره چک لیست ',
    'Checklists'=>'چک لیست ها',
    'ChecklistsNetwork'=>'چک لیست های بخش شبکه',
    'ChecklistsSoftware'=>'چک لیست های بخش نرم افزار',
    'ChecklistsHardware'=>' چک لیست های بخش سخت افزار',
    'analyzeChecklists'=>'ارزیابی فنی چک لیست ها',
     'about us'=>'درباره ما',
    'login and register'=>'ثبت نام / ورود',
     'QA_TITLE'=>'سامانه دستیار ارزیابی امنیتی زنجیره بلوکی(اتریوم)',
      'QA_TITLE_DESCRIPTION'=>'این سامانه جهت تسهیل در روند ارزیابی امنیتی زنجیره بلوکی (اتریوم) طراحی شده است.',
      'start'=>'شروع به کار',
    'login'=>'ورود',
    'register'=>'ثبت نام',
    'logout'=>'خروج',
    'profile'=>' پروفایل',
    'panel'=>' پروفایل',
    'Admin'=>' مدیر',
    'twoFactor'=>'احراز هویت دو مرحله ای',


];
