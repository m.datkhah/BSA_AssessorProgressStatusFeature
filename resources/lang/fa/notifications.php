<?php
return[
  'notification'=>'اطلاع رسانی',
  'sendEmail'=>'ارسال ایمیل',
  'sendSms'=>'ارسال پیام کوتاه',
  'users'=>'کاربرها',
  'Email Types'=>'نوع ایمیل',
  'Sms text'=>'متن پیام کوتاه',
  'Send'=>'ارسال',
  'email_send_success'=>'ایمیل با موفقیت ارسال شد.',
  'email_has_problem'=>'سرویس ایمیل با مشکل مواجه شده است. لطفا دقایق بعد سعی کنید.',

];
