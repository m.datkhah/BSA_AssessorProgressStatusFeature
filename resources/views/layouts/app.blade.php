<!DOCTYPE html>
<html dir="rtl" lang="fn">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    @include('links.link')
    <link rel="stylesheet" href="{{asset('css/bootstraprtl-v4.css')}}">
    <link rel="stylesheet" href="{{asset('css/style1.css')}}">
    <script src="{{asset('js/app.js')}}"></script>
    <title>@yield('title' , 'Laravel')</title>
</head>

<body>
<!-- Nav Menu -->
<div class=" container">
    @include('partials.navbar')

    @include('partials.alerts')

    @include('partials.hero')

</div>
<div class="container"></div>
</body>

</html>

