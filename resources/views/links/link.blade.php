
<link rel="stylesheet" href="{{asset('css/style1.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('css/montserrat-font.css')}}">
<link rel="stylesheet" href="{{asset('css/vendor/bootstrap.min.css')}}">
<link rel="stylesheet" href="{{asset('css/vendor/modal-box/bootstrap-side-modals.css')}}">
<link href="{{asset('css/main.css')}}" rel="stylesheet" />
<script src="{{asset('js/search-3/extention/choices.js')}}"></script>
<link rel="stylesheet" href="{{asset('css/pure.css')}}" type="text/css"/>
<link href="{{asset('css/vendor/bootstrap-icons/bootstrap-icons.css')}}" rel="stylesheet">
<link href="{{asset('css/vendor/boxicons/boxicons.min.css')}}" rel="stylesheet">
<link href="{{asset('css/vendor/glightbox/glightbox.min.css')}}" rel="stylesheet">
<link href="{{asset('css/vendor/swiper/swiper-bundle.min.css')}}" rel="stylesheet">
<link href="{{asset('css/style2.css')}}" rel="stylesheet">
