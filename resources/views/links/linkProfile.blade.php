<!-- Bootstrap CSS -->
<link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet">
<!-- bootstrap theme -->
<link href="{{asset('css/bootstrap-theme.css')}}" rel="stylesheet">
<!--external css-->
<!-- font icon -->
<link href="{{asset('css/elegant-icons-style.css')}}" rel="stylesheet" />
<link href="{{asset('css/font-awesome.min.css')}}" rel="stylesheet" />
<!-- full calendar css-->
<link href="{{asset('css/fullcalendar/bootstrap-fullcalendar.css')}}" rel="stylesheet" />
<link href="{{asset('css/fullcalendar/fullcalendar.css')}}" rel="stylesheet" />
<!-- easy pie chart-->
<link href="{{asset('css/jquery-easy-pie-chart/jquery.easy-pie-chart.css')}}" rel="stylesheet" type="text/css" media="screen" />
<!-- owl carousel -->
<link rel="stylesheet" href="{{asset('css/owl.carousel.css')}}" type="text/css">
<link href="{{asset('css/jquery-jvectormap-1.2.2.css')}}" rel="stylesheet">
<!-- Custom styles -->
<link rel="stylesheet" href="{{asset('css/fullcalendar.css')}}">
<link href="{{asset('css/widgets.css')}}" rel="stylesheet">
<link href="{{asset('css/style.css')}}" rel="stylesheet">
<link href="{{asset('css/style1.css')}}" rel="stylesheet">
<link href="{{asset('css/style-responsive.css')}}" rel="stylesheet" />
<link href="{{asset('css/xcharts.min.css')}}" rel=" stylesheet">
<link href="{{asset('css/jquery-ui-1.10.4.min.css')}}" rel="stylesheet">
<link rel="shortcut icon" href="{{asset('css/line-icons')}}">
