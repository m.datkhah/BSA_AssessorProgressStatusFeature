@extends('layouts.profile')


@section('content')

    <style>
        input[type=text], select, textarea {
            width: 100%;
            padding: 12px;
            border: 1px solid #ccc;
            border-radius: 4px;
            resize: vertical;
        }

        label {
            padding: 12px 12px 12px 0;
            display: inline-block;
        }

        input[type=submit] {
            background-color: #04AA6D;
            color: white;
            padding: 12px 20px;
            border: none;
            border-radius: 4px;
            cursor: pointer;
            float: right;
        }

        input[type=submit]:hover {
            background-color: #45a049;
        }

        .container2 {
            border-radius: 5px;
            background-color: #f2f2f2;
            padding: 20px;
            width: 52%;
            margin-right: 28%;
            font-family: "B Nazanin";
        }

        .col-25 {
            float: left;
            width: 25%;
            margin-top: 6px;
        }

        .col-75 {
            float: left;
            width: 75%;
            margin-top: 6px;
        }

        /* Clear floats after the columns */
        .row:after {
            content: "";
            display: table;
            clear: both;
        }

        /* Responsive layout - when the screen is less than 600px wide, make the two columns stack on top of each other instead of next to each other */
        @media screen and (max-width: 600px) {
            .col-25, .col-75, input[type=submit] {
                width: 100%;
                margin-top: 0;
            }
        }
    </style>

    <div class="col-lg-12">
        <ol class="breadcrumb">
            <li><i class="fa fa-home"></i><a href="index.html">@lang('panel.home')</a></li>
            <li></i><a href="{{route('Auth.show.checklist')}}">@lang('panel.My checklists')</a></li>
            <li></i>@lang('panel.software')</li>
            <li></i>@lang('panel.AddChecklists')</li>
        </ol>
    </div>

    <div class="container2">
        @csrf
        @include('partials.alerts')
        <form action="{{route('Auth.store.checklist')}}" method='Post'>
            <div class="row">
                <div class="col-25">
                    <label for="network">@lang('panel.categoryAttack')</label>
                </div>
                <div class="col-75">
                    <select id="network" name="network">
                        <option value="1111">@lang('panel.Consensus algorithm')</option>
                    </select>
                </div>
            </div>

            <div class="row">
                <div class="col-25">
                    <label for="subject">@lang('panel.Subject')</label>
                </div>
                <div class="col-75">
                    <textarea id="subject" name="text" placeholder="متن سوال را وارد کنید..."
                              style="height:200px"></textarea>
                </div>
            </div>
            <div class="row">
                <input type="submit" style="font-family: 'B Nazanin'" value="ثپت سوال">
            </div>
            <div class="offset-sm-3">
                @include('partials.validation-errors')
            </div>
        </form>
    </div>
@endsection
