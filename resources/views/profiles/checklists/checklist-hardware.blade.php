@extends('layouts.profile')


@section('content')
    <style>
        .page-title {
            color: #324a62;
            margin-top: 11px;
            font-weight: 500;
            line-height: 1.3;
            font-size: 1.6rem;
        }

        .w-auto {
            width: auto !important;
        }

        .text-truncate {
            overflow: hidden;
            text-overflow: ellipsis;
            white-space: nowrap;
        }

        .topnav {
            overflow: hidden;
            /*  background-color: #e9e9e9;*/
            font-family: "B Nazanin";
        }

        .topnav a {
            float: left;
            display: block;
            color: black;
            text-align: center;
            padding: 14px 16px;
            text-decoration: none;
            font-size: 17px;
        }

        .topnav a:hover {
            background-color: #ddd;
            color: black;
        }

        .topnav a.active {
            background-color: #2196F3;
            color: white;
        }

        .topnav .search-container {
            float: right;
        }

        .topnav input[type=text] {
            padding: 6px;
            margin-top: 8px;
            font-size: 17px;
            border: none;
        }

        .topnav .search-container button {
            float: right;
            padding: 6px 10px;
            margin-top: 8px;
            margin-right: 16px;
            background: #ddd;
            font-size: 17px;
            border: none;
            cursor: pointer;
        }

        .topnav .search-container button:hover {
            background: #ccc;
        }

        @media screen and (max-width: 600px) {
            .topnav .search-container {
                float: none;
            }

            .topnav a, .topnav input[type=text], .topnav .search-container button {
                float: none;
                display: block;
                text-align: left;
                width: 100%;
                margin: 0;
                padding: 14px;
            }

            .topnav input[type=text] {
                border: 1px solid #ccc;
            }
        }

        .flex-container {
            display: flex;
            background-color: #f0f2f5;
        }

        .flex-container > div {
            background-color: #f1f1f1;
            width: 100px;
            margin: 10px;
            text-align: center;
            line-height: 75px;
            font-size: 30px;
        }

        .panel, .flip {

            padding: 5px;
            text-align: center;
        }

        .flip {
            width: 144px;
            font-size: 24px;
        }

        .panel {
            font-size: 24px;
            direction: ltr;
            width: 207px;
            padding: 50px;
            display: none;
        }

        /*-----------------------------------------------*/
        .accordion {
            background-color: #eee;
            color: #444;
            cursor: pointer;
            padding: 18px;
            width: 100%;
            border: none;
            text-align: right;
            outline: none;
            font-size: 15px;
            transition: 0.4s;
            font-size: 26px;
        }

        .active, .accordion:hover {
            background-color: #ccc;
        }

        .panel2 {
            padding: 0 18px;
            display: none;
            background-color: white;
            overflow: hidden;
        }
    </style>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <div class="app-page-container" id="app-page-container">
        <div class="col-lg-6" style="float: right">
            <h1 class="page-title text-truncate w-auto">@lang('panel.My checklists')</h1>
        </div>
        <div class="col-lg-4">
            <div class="topnav">
                <div class="search-container">
                    <form action="/action_page.php">
                        <input type="text" placeholder="@lang('panel.Search..')" name="search">
                        <button type="submit"><i class="fa fa-search"></i></button>
                    </form>
                </div>
            </div>

        </div>
    </div>
    <div class="flex-container" style="margin-top: 51px;height: 93%">
        <div style="width: 20%; border: 1px solid #c2c2c2;">
            <button class="accordion">
                <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor"
                     class="bi bi-folder" viewBox="0 0 16 16" style=" margin-left: 3px;">
                    <path
                        d="M.54 3.87.5 3a2 2 0 0 1 2-2h3.672a2 2 0 0 1 1.414.586l.828.828A2 2 0 0 0 9.828 3h3.982a2 2 0 0 1 1.992 2.181l-.637 7A2 2 0 0 1 13.174 14H2.826a2 2 0 0 1-1.991-1.819l-.637-7a1.99 1.99 0 0 1 .342-1.31zM2.19 4a1 1 0 0 0-.996 1.09l.637 7a1 1 0 0 0 .995.91h10.348a1 1 0 0 0 .995-.91l.637-7A1 1 0 0 0 13.81 4H2.19zm4.69-1.707A1 1 0 0 0 6.172 2H2.5a1 1 0 0 0-1 .981l.006.139C1.72 3.042 1.95 3 2.19 3h5.396l-.707-.707z"/>
                </svg>@lang('panel.hardware')</button>
            <div class="panel2">
                <form action="{{route('app.show.checklist.hardware')}}" method='post'>
                    <input type="hidden" name="type" value="111111">
                    <button type="submit"
                            style="width:107%;font-size: 70%;text-align: right;border: none;border-radius: 12px">@lang('panel.Wallet attack')</button>
                </form>
            </div>
            <script>
                var acc = document.getElementsByClassName("accordion");
                var i;

                for (i = 0; i < acc.length; i++) {
                    acc[i].addEventListener("click", function () {
                        this.classList.toggle("active");
                        var panel = this.nextElementSibling;
                        if (panel.style.display === "block") {
                            panel.style.display = "none";
                        } else {
                            panel.style.display = "block";
                        }
                    });
                }
            </script>

        </div>

    </div>
    </div>
@endsection
