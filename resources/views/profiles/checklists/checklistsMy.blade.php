@extends('layouts.profile')


@section('content')
    <style>
        .page-title {
            color: #324a62;
            margin-top: 11px;
            font-weight: 500;
            line-height: 1.3;
            font-size: 1.6rem;
        }

        .w-auto {
            width: auto !important;
        }

        .text-truncate {
            overflow: hidden;
            text-overflow: ellipsis;
            white-space: nowrap;
        }

        .topnav {
            overflow: hidden;
            font-family: "B Nazanin";
        }

        .topnav a {
            float: left;
            display: block;
            color: black;
            text-align: center;
            padding: 14px 16px;
            text-decoration: none;
            font-size: 17px;
        }

        .topnav a:hover {
            background-color: #ddd;
            color: black;
        }

        .topnav a.active {
            background-color: #2196F3;
            color: white;
        }

        .topnav .search-container {
            float: right;
        }

        .topnav input[type=text] {
            padding: 6px;
            margin-top: 8px;
            font-size: 17px;
            border: none;
        }

        .topnav .search-container button {
            float: right;
            padding: 6px 10px;
            margin-top: 8px;
            margin-right: 16px;
            background: #ddd;
            font-size: 17px;
            border: none;
            cursor: pointer;
        }

        .topnav .search-container button:hover {
            background: #ccc;
        }

        @media screen and (max-width: 600px) {
            .topnav .search-container {
                float: none;
            }

            .topnav a, .topnav input[type=text], .topnav .search-container button {
                float: none;
                display: block;
                text-align: left;
                width: 100%;
                margin: 0;
                padding: 14px;
            }

            .topnav input[type=text] {
                border: 1px solid #ccc;
            }
        }

        .flex-container {
            display: flex;
            background-color: #f0f2f5;
        }

        .flex-container > div {
            background-color: #f1f1f1;
            width: 100px;
            margin: 10px;
            text-align: center;
            line-height: 75px;
            font-size: 30px;
        }

        /* -------------------------------------------------*/
        .container5 {
            direction: ltr;
            width: 100%;
            background-color: #ddd;
        }

        .skills {
            text-align: right;
            padding-top: 10px;
            padding-bottom: 10px;
            color: #1a1919;
        }

        .html {
            width: 10%;
            background-color: #04AA6D;
        }
    </style>
    <!-- ---------------- files checklistsMy.blade-------------------- -->

    <div class="app-page-container" id="app-page-container">
        <div class="col-lg-6" style="float: right">
            <h1 class="page-title text-truncate w-auto">@lang('panel.My checklists')</h1>
        </div>

        <div class="col-lg-4">
            <div class="topnav">
                <div class="search-container">
                    <form action="/action_page.php">
                        <input type="text" placeholder="@lang('panel.Search..')" name="search">
                        <button type="submit"><i class="fa fa-search"></i></button>
                    </form>
                </div>
            </div>

        </div>
    </div>

    <div class="flex-container" style="margin-top: 69px">
        <div style="width: 25%">
            <div class="pure-g">
                <div class="pure-u-1-2"><img src="https://up.7Learn.com/m/bnr/4-kh7.png" alt="لاراول کاربردی"
                                             height="100px" width="200px"></div>
                <div class="pure-u-1-2">
                    <a class="topic"
                       href="{{route('Auth.show.checklists.Network')}}"><span>@lang('panel.network')</span></a>
                    <span>/</span><a class="topic"
                                     href="{{route('Auth.ShowFormAdd.checklists.Network')}}"><span>@lang('panel.AddChecklists')</span></a>
                    <span style="text-align: center"><h3>@lang('panel.NotCompleted')</h3></span>
                    <div class="container5">
                        <div class="skills html" style="height: 10px">10%</div>
                    </div>
                </div>
            </div>
        </div>
        <div style="width: 25%">
            <div class="pure-g">
                <div class="pure-u-1-2"><img src="https://up.7Learn.com/m/bnr/4-kh7.png" alt="لاراول کاربردی"
                                             height="100px" width="200px"></div>
                <div class="pure-u-1-2">
                    <a class="topic"
                       href="{{route('Auth.show.checklists.Software')}}"><span>@lang('panel.software')</span></a>
                    <span>/</span><a class="topic"
                                     href="{{route('Auth.ShowFormAdd.checklists.software')}}"><span>@lang('panel.AddChecklists')</span></a>
                    <span style="text-align: center"><h3>@lang('panel.NotCompleted')</h3></span>
                    <div class="slidecontainer" style="direction: ltr">
                        <span>8%</span>
                        <input type="range" min="1" max="100" value="10">
                    </div>
                </div>
            </div>
        </div>
        <div style="width: 25%">
            <div class="pure-g">
                <div class="pure-u-1-2"><img src="https://up.7Learn.com/m/bnr/4-kh7.png" alt="لاراول کاربردی"
                                             height="100px" width="200px"></div>
                <div class="pure-u-1-2">
                    <a class="topic"
                       href="{{route('Auth.show.checklists.Hardware')}}"><span>@lang('panel.hardware')</span></a>
                    <span>/</span><a class="topic"
                                     href="{{route('Auth.ShowFormAdd.checklists.hardware')}}"><span>@lang('panel.AddChecklists')</span></a>
                    <span style="text-align: center"><h3>@lang('panel.NotCompleted')</h3></span>
                    <div class="slidecontainer" style="direction: ltr">
                        <span>8%</span>
                        <input type="range" min="1" max="100" value="10">
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
