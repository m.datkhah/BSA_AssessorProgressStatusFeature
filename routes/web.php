<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {

    return view('layouts.app');
})->name('home');

Route::group(['prefix'=>'Auth', 'namespace'=>'Auth'],function () {

    Route::get('register','RegisterController@ShowFormRegister')->name('Auth.show.register.form');
    Route::post('register','RegisterController@register')->name('Auth.register');
    Route::get('login','LoginController@showFormLogin')->name('Auth.showLoginForm');
    Route::post('login','LoginController@Login')->name('Auth.LoginUser');
    Route::get('logout','LoginController@logout')->name('Auth.logout');
    });
Route::group(['prefix'=>'app', 'namespace'=>'app'],function () {
    Route::get('profile','profileController@showProfile')->name('Auth.show.profile');
    Route::get('checklists','profileController@showChecklists')->name('Auth.show.checklist');
    Route::get('checklists/practical-network','profileController@showChecklistsNetwork')->name('Auth.show.checklists.Network');
    Route::get('checklists/practical-network/add','profileController@ShowFormAddChecklistsNetwork')->name('Auth.ShowFormAdd.checklists.Network');
    Route::post('storeChecklist','profileController@storeChecklist')->name('Auth.store.checklist');
    Route::post('storeResponse','profileController@storeResponse')->name('app.store.Response');
    Route::get('badge/new','profileController@newBadge')->name('app.new.badge');
    Route::post('badge','profileController@storeBadge')->name('app.store.badge');
    Route::get('responses/{response}/remove','profileController@removeResponse')->name('app.remove.response');
    Route::post('practical-network/show/network','profileController@showChecklistNetwork')->name('app.show.checklist.network');
    Route::post('practical-hardware/show/hardware','profileController@showChecklistHardware')->name('app.show.checklist.hardware');
    Route::post('practical-software/show/software','profileController@showChecklistSoftware')->name('app.show.checklist.software');
    Route::get('checklists/practical-software/add','profileController@ShowFormAddChecklistsSoftware')->name('Auth.ShowFormAdd.checklists.software');
    Route::get('checklists/practical-hardware/add','profileController@ShowFormAddChecklistsHardware')->name('Auth.ShowFormAdd.checklists.hardware');
    Route::get('checklists/practical-software','profileController@showChecklistsSoftware')->name('Auth.show.checklists.Software');
    Route::get('checklists/practical-hardware','profileController@showChecklistsHardware')->name('Auth.show.checklists.Hardware');
});

